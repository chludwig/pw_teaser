.. include:: ../Includes.txt

.. _support:


Support
=======

Issue tracker
-------------
See all open tasks `here`_ in the forge bug tracker.

.. _here: https://forge.typo3.org/projects/extension-pw_teaser/issues


Donate
------
If you like the pw_teaser extension, feel free to `donate`_ some funds to support further development.

.. _donate: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=2DCCULSKFRZFU


Contribute
----------
If you are a developer and you want to submit improvements as code, you can fork https://bitbucket.org/ArminVieweg/pw_teaser/src
and make a pull request to pw_teaser's master branch.

Thanks!
