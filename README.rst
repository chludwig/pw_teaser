pw_teaser for TYPO3 CMS
=======================

Features
--------

* Create nested or flat views of your page structures
* extreme detailed options to filter pages available
* layout the teasers of your pages like you want with Fluid Templates
* template presets
* pagination is also supported
* extendable with signals to modify or extend page models


Documentation
-------------

This extension provides a ReST documentation, located in Documentation/ directory.

You can see a rendered version on https://docs.typo3.org/p/t3/pw_teaser


Requirements
------------

Version 5.0 of pw_teaser requires TYPO3 9.5 or 10.4.


Links
-----

* **Donate:** https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=9LJYFQGJ7S232
* Issue Tracker: https://forge.typo3.org/projects/extension-pw_teaser/issues
* Source code: https://bitbucket.org/ArminVieweg/pw_teaser
